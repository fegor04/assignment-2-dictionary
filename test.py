#!/usr/bin/python3
import unittest
import xmlrunner
import subprocess

__unittest = True

SEGFAULT_EXIT_CODE = -11
ERROR_EXIT_CODE = 42


class DictionaryUnitTest(unittest.TestCase):
    def tearDown(self):
        self.write_words([])

    def write_words(self, words):
        with open("words.inc", "w") as f:
            f.write("; vim:ft=nasm\n%include \"colon.inc\"\nsection .rodata\n")
            for i in range(len(words)):
                word = words[i]
                f.write(f"colon \"{word}\", w{i}\n")
                f.write(f"db \"{word} = {i}\", 0\n")
            f.close()

    def compile(self):
        subprocess.run(["make", "clean"])
        subprocess.run(["make", "debug"], check=True)

    def get_value(self, key):
        output = b''
        try:
            p = subprocess.Popen(['./main.debug'], shell=None,
                                 stdin=subprocess.PIPE,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
            (output, stderr) = p.communicate(key.encode())
            self.assertNotEqual(
                    p.returncode,
                    SEGFAULT_EXIT_CODE,
                    'segmentation fault'
                    )
            return (output.decode(), stderr.decode(), p.returncode)
        except subprocess.CalledProcessError as exc:
            self.assertNotEqual(
                    p.returncode,
                    SEGFAULT_EXIT_CODE,
                    'segmentation fault'
                    )
            return (exc.output.decode(), exc.stderr.decode(), exc.returncode)

    def test_empty_dict(self):
        self.write_words([])
        self.compile()
        (_, emptyValue, code) = self.get_value("KEY")
        self.assertEqual(emptyValue, "Exception: key not found.")
        self.assertEqual(code, ERROR_EXIT_CODE)

    def test_not_empty_dict(self):
        self.write_words(["Goodbye", "Hi", "Shalom"])
        self.compile()
        (_, emptyValue, code) = self.get_value("KEY")
        self.assertEqual(emptyValue, "Exception: key not found.")
        self.assertEqual(code, ERROR_EXIT_CODE)
        (out, _,  code) = self.get_value("Hi")
        self.assertEqual(out, "Hi = 1")
        self.assertEqual(code, 0)

    def test_read_error(self):
        self.write_words(["Goodbye", "Hi", "Shalom"])
        self.compile()
        (_, emptyValue, code) = self.get_value("")
        self.assertEqual(emptyValue, "Exception: could not read from stdin")
        self.assertEqual(code, ERROR_EXIT_CODE)

    def test_too_long_key(self):
        long_key = "A" * 300
        self.write_words([long_key])
        self.assertRaises(subprocess.CalledProcessError, self.compile)

    def test_ok_size_key(self):
        sizes = [50, 100, 200, 255]
        for size in sizes:
            key = "A" * size
            self.write_words([key])
            self.compile()
            (stdout, _,  code) = self.get_value(key)
            self.assertEqual(stdout, f"{key} = 0")
            self.assertEqual(code, 0)


if __name__ == "__main__":
    with open('report.xml', 'w') as report:
        unittest.main(
                testRunner=xmlrunner.XMLTestRunner(output=report),
                failfast=False, buffer=False, catchbreak=False
                )
