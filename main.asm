; vim:ft=nasm

%define dict_head 0
%define MAX_KEY_LENGTH 255

%define EXIT_CODE_ERROR 42
%include "lib.inc"
%include "words.inc"
%include "dict.inc"

section .bss
buffer: resb (MAX_KEY_LENGTH + 1)

section .rodata
key_not_found_message: db "Exception: key not found.", 0
read_error_message: db 'Exception: could not read from stdin', 0

section .text
global _start

_start:
    xor rax, rax
    xor rdi, rdi
    lea rsi, [buffer]
    mov rdx, MAX_KEY_LENGTH
    syscall
    test rax, rax
    jle .read_error
    lea  rdi, [rax+buffer]
    mov  byte[rdi], 0
    mov  rdi, buffer
    lea  rsi, [dict_head]
    call find_word
    test rax, rax
    jz   .assembly.lang.key_not_found_exception
    mov  rdi, rax
    call get_value
    mov  rdi, rax
    call print_string
    xor rdi, rdi
    call exit 
    .assembly.lang.key_not_found_exception:
    lea rdi, [key_not_found_message]
    call print_error
    mov  rdi, EXIT_CODE_ERROR
    call exit
    .read_error:
    lea rdi, [read_error_message]
    call print_error
    mov  rdi, EXIT_CODE_ERROR
    call exit


