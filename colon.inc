; vim:ft=nasm
%macro colon 2 ; value, label
    %strlen key_len %1
    %if key_len <= MAX_KEY_LENGTH
        %2:
        dq dict_head
        db %1, 0
        %define dict_head %2
    %else
        %error Error: entry key is too long. key_len > 255
    %endif
%endmacro
