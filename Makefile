ASM=nasm
ASMFLAGS=-f elf64
LD=ld

all: main main.debug main.preprocessor
debug: main.debug

preprocessor: main.preprocessor

main: lib.o dict.o main.o
	$(LD) -o main main.o lib.o dict.o 

main.debug: lib.debug.o dict.debug.o main.debug.o
	$(LD) -o main.debug main.debug.o lib.debug.o dict.debug.o 

main.preprocessor: lib.preprocessor.o dict.preprocessor.o main.preprocessor.o colon.inc words.inc
	echo "All ASM files preprocessed!"

test: debug test.py
	python3 ./test.py

main.o: main.asm lib.inc words.inc dict.inc
dict.o: dict.asm lib.inc
words.inc: colon.inc

%.o: %.asm
	$(ASM) $(ASMFLAGS) $< -o $@

%.debug.o: %.asm
	$(ASM) $(ASMFLAGS) -g $< -o $@

%.preprocessor.o: %.asm
	$(ASM) $(ASMFLAGS) -E $< -o $@

.PHONY: clean 
clean:
	rm *.o

