; vim:ft=nasm
%include "lib.inc"

section .text
global find_word
global get_value
; Проходит по словарю в поисках вхождения с заданным ключом.
; Если вхождение существует, возвращает адрес начала вхождения в словарь.
; Если вхождение не существует, возвращает 0;
; @param rdi - указатель на нуль-терминированную строку
; @param rsi - указатель на начало словаря
find_word:
    push    rbp
    mov     rbp, rsp

    test    rsi, rsi
    jz      .ret_no

    push    rdi
    push    rsi
    
    lea     rsi, [rsi + 8]
    call    string_equals
    pop     rsi
    pop     rdi
    test    rax, rax
    jnz     .ret
    mov     rsi, [rsi]
    test    rsi, rsi
    jz      .ret_no
    call    find_word
    leave
    ret
    .ret:
    lea     rax, [rsi]
    leave
    ret
    .ret_no:
    xor     rax, rax
    leave
    ret

; Принимает указатель на вхожедние в словарь.
; Вовзращает указатель на значение в данном вхождении.
; rdi - pointer
get_value:
    push    rbp
    mov     rbp, rsp
    sub     rsp, 24 ; 24 + 8 == 32, ABI
    lea     rdi, [rdi+8] ; key
    mov     [rbp], rdi
    call    string_length
    mov     rdi, [rbp]
    add     rax, rdi
    inc     rax
    leave
    ret
